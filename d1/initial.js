// CRUD Operetaions
/*
	- CRUD is an acronym for: Create, Read, Update and Deelete
	- Create- allows new users to create new record in database
	- Read- similar to search function. It allows users to search and retrieve specific records
	- Update- used to modify existing records 
	- Delete- allows users to remove records from a database that is no longer needed

*/

// CREATE: Insert Documents
/*
	- the mongo shell uses JavaScript for its syntax
	MOngoDB deals with objects as it's structure for documents
	We can create documents by providing objects into our methods
	JavaScript SYNTAX
		object.object.method({object})
*/

// INSERT ONE
/*
	SYNTAX 
		db.collectionName.insertOne({});
*/

db.users.insertOne({
	fisrtName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321"
		email: "janedoe@gmail.com"
	}
});

// INSERT MANY
/*
	SYNTAX
		db.collectionName.insertMany([{objectA}, {ibjectB}])
*/

db.users.insertMany([
		{
			firstName: "Stepehen",
			lastName: "Hawkings",
			age: 82,
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
		},
		course: ["Python", "React", "PHP"],
		department: "none"
		},

		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
	contact: {
		phone: "87654321",
		email: "neilarmstrong@gmail.com"
		},
		course: ["React", "Laravel", "Sass"]
		}
	]);



// REAd: Retrieve/find documents
/*
	- documents will be returned

	SYNTAX
		db.users.find();
*/


// FIND using single parameter
/*
	SYNTAX
		db.collectionName.find({field: value})
*/

db.users.find({firstName: "Stephen"});

// FIND using multiple parameter
/*
	SYNTAX
		db.collectionName.find({fieldA: valueA, fieldB: valueB})
*/
db.users.find({lastName: "Armstrong", age: 82});

// FIND + PRETTY METHOD
/*
	- the "pretty" method allows us to be able to view the documents returned by our terminal in a "prettier" format
	SYNTAX
		db.collectionName.find({field: vale}).pretty();
*/

db.users.find({lastName: "Armstrong", age: 82}).pretty();

// UPDATE - editing of document
/*
	- updating a single document
	SYNTAX
		db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/

// For example, let us cretae a document that we will then update

	// 1. insert initial document


		db.users.insertOne({
			firstName: "Test",
			lastName: "Test",
			age: 0,
			contact: {
				phone: "0000000",
				email: "test@gmail.com"
			},
			courses: [],
			department: "none"
		});

	// 2. update the document

			db.users.updateOne(
		{ 
			"firstName": "Test"
		},
		{
			$set: {
				firstName: "Bill",
				lastName: "Gates",
				"age": 65,
				contact: {
					phone: "87654321",
					email: "bill@gmail.com"
				},
				course: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
		);

// UPDATE MANY - updating multiple documents
/*
	SYNTAX
		db.collectionName.updateMany( {criteria},  {$set{field: value}});
*/

			db.users.updateMany(
				{department: "none"},
				{
					$set: { department: "HR"}
				}
			);


// REPLACE ONE
/*
	- replaces the whole document
	- if updateOne updates a specific field, replaceOne replaces the whole document
	- if updateOne updates parts, replaceOne replaces the whole document
*/

db.users.replaceOne(
		{ fisrtName: "Bill"},
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@rocketmail.com",
			},
			course: ["PHP", "Laravel", "HTML"],
			department: "Operations"

		}
	);

db.users.find({firstName: "Bill"});


// DELETING DOCUMENTS
// For example, lets create a document that we will delete
// It is good to practice soft deletion/archiving instead of deleting or removing completely from the system 

	db.users.insertOne({
			firstName: "Test",
		});

	// DELETING a single document
	// SYNTAX - db.collectionName.deleteOne({criteria})

	db.users.deleteOne({
		firstName: "Test"
	});

	db.users.find({firstName: "Test"}).pretty();


// DELETING MANY
// SYNTAX - db.collectionName.deleteMany({criteria});

db.users.deleteMany(
		{firstName: "Bill"}
	);

db.users.find({firstName: "Bill"}).pretty();

db.users.insertMany([
	{firstName: "Bill",
	lastName: "Magalang",
	age: 12
	},
	{firstName: "Bill",
	lastName: "Batumbakal",
	age: 13
	}
]);


// DELETE ALL - deleteing all documents
// SYNTAX - db.collectionName.deleteMany({});


// ADVANCED QUERIES
/*
	- retrieving data with complex data structures is also a good skill for any developer to have
	- real world examples of data can be as complex as having two ormore layers of nested objects (object inside an object)
	- learning to qeuery these kinds of data is also essential to esnure that we are able to retrieve any information that we would need in our application
*/ 

// Query an embedded document
// - an embedded docs are those types of documents that contain a document inside a document (document inside a document) 

db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
}).pretty();


// Query on nested field

db.users.find({
	"contact.email": "janedoe@gmail.com"
}).pretty();

// Querying an array with eaxct elements
db.users.find({
	courses: ["CSS", "Java", "Python"]
}).pretty();

// Querying an array without regard to order
db.users.find({
	courses: {$all: ["React", "Python",]}
}).pretty();

// Querying on embedded array
db.users.insert({
	namearr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
	]

});
db.users.find({
	namearre
	{
		nameA: "Juan"
	}
}).pretty();

